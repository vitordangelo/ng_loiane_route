import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AlunosGuard implements CanActivateChild {
    
	canActivateChild(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
		// console.log('Guarda de rota filha');
		// console.log(route);
		// console.log(state);

		console.log('AlunosGuard: Guarda de Rota Filha');
		

		// if(state.url.includes('editar')){
		// 	alert('Você não tem permissão');
		// 	return false;
		// }	
		
		return true;
	}
}