import { L07RotasPage } from './app.po';

describe('l07-rotas App', function() {
  let page: L07RotasPage;

  beforeEach(() => {
    page = new L07RotasPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
